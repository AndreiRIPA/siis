﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Login_SIIS.Models.Pacient
{
    public class PacientRepository : Controller
    {
        PacientEntities db = new PacientEntities();

        public IEnumerable<Pacient> getAllPacients()
        {
            return db.Pacients.ToList();
        }

        public Pacient getPacientByCNP(int cnp)
        {
            return db.Pacients.Where(x => x.CNP == cnp + "").FirstOrDefault();
        }

    }
}