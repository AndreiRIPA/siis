﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Login_SIIS.Startup))]
namespace Login_SIIS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
