﻿using Login_SIIS.Models.Pacient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Login_SIIS.Controllers.Central
{
    public class PacientController : Controller
    {
        PacientRepository pacientRepo = new PacientRepository();

        public IEnumerable<Pacient> Index()
        {
            return pacientRepo.getAllPacients();
        }

        public Pacient getPacientByCNP(int cnp)
        {
            return pacientRepo.getPacientByCNP(cnp);
        }

 
    }
}